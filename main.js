const toTranslate = document.querySelectorAll('[translate]')
const language = navigator.language

let sentences = [
  {name: 'MAIN_INFO', sentences: 'On Friday 28th june an international occupy will happen in Paris'},
  {name: 'MORE_INFOS', sentences: 'More informations'},
  {name: 'IMG', sentences: '<img src="img/occupy-en.png" width="450"/>'},
  {name: 'TEXT_OCCUPY', sentences: `France has an important place in the world, since it has an important place in the world (member of the UNO security council, of the G7, historical country in the EU). That's why this occupation is international. And no, France is not a model in ecology, since our CO2 emissions is increasing by 3% each year.
  <br>
  <br><a href="https://hebdo.framapad.org/p/28juacten" target="_blank">More informations</a>
  <br><a href="https://framaforms.org/register-form-international-occupy-for-climate-paris-28-june-1559910479" target="_blank">Register</a>`},
]

if (!language.includes('fr')) {
  for (a in toTranslate) {
    if (toTranslate[a].attributes !== undefined) {

      for (b in sentences) {
        if (toTranslate[a].attributes.translate.nodeValue === sentences[b].name) {
          toTranslate[a].innerHTML = sentences[b].sentences
        }
      }

    }
  }
}
